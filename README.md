# Modified Charge-Transfer Ionic Potential

The repository contains modifications to the LAMMPS charge-equilibration fix and Streitz-Mintmire pair style, along with new header and source files for the modified charge-transfer ionic potential (CTIP) as developed by [Zhou and Wadley](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.69.035402). It builds off on top of their publication that discusses CTIP for the [O-Al-Ni-Co-Fe material system](https://iopscience.iop.org/article/10.1088/0953-8984/17/23/014). Example input and parameter files for the Hf/Cu/O system are also provided.

This work was done as part of the following publications (please cite either one of these in publications that generate results using this repository or its derivatives):
- [Chem. Mater. 2017, 29, 8, 3603–3614](https://doi.org/10.1021/acs.chemmater.7b00312)
- [Chem. Mater. 2019, 31, 9, 3089–3102](https://doi.org/10.1021/acs.chemmater.8b03969)

The provided source and header files were originally implemented in the 10Aug2015 version of LAMMPS and has not been ported to a newer version yet.

